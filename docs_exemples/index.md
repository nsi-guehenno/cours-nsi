---
author: Votre nom
title: 🏡 Accueil
---

A vous de personnaliser cet accueil

!!! info "Adapter ce site modèle"

    Le tutoriel est ici : [Tutoriel de site avec python](https://docs.forge.apps.education.fr/tutoriels/pyodide-mkdocs-theme-review/){:target="_blank" }
    
    Si vous voulez conserver certaines pages de ce modèles sans qu'elles ne soient visibles dans le menu, il suffit de les enlever du fichier .pages   
    Vous les retrouverez facilement en utilisant la barre de recherche en haut à droite
    

😊  Bienvenue !




